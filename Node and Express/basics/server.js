import express from "express";
import bodyParser from "body-parser"; // allows to get body data from frontend
import usersRoutes from "./routes/user.js";

const app = express();
const PORT = process.env.PORT || 5000;

// middlewares
app.use(bodyParser.json());

// routes middlewares
// set the starting path for all the routes in the users file...
app.use('/users', usersRoutes);

app.get("/", (req, res) => {
    res.send("This is Home Route");
})

app.listen(PORT, () => console.log(`Server running at port: ${PORT}`));