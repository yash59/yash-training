import express from "express";
import { v4 as uuidv4 } from "uuid"; // to generate random unique id

const router = express.Router();

let users = [];

// define your routes
// all routes here are starting with /users

// get all users data
router.get("/", (req, res) => {
    res.send(users);
})

// get user data with a specific id
router.get("/:id", (req, res) => { // ":" means anything after ':'
    const { id: userId } = req.params;
    const userFound = users.find((user) => user.id === userId);
    res.send(userFound);
})

// create a user
router.post("/", (req, res) => {
    const userData = req.body;
    users.push({...userData, id:uuidv4()});
    res.send(`user with name ${userData.firstName} was added to the database.`);
})

// delete user with specific id
router.delete("/:id", (req, res) => {
    const { id: userId } = req.params;
    users = users.filter(user => user.id !== userId);
    res.send(`User with the id: ${userId} deleted successfully.`);
})

// update (patch - partially update) a user with specifc id
router.patch("/:id", (req, res) => {
    const { id: userId } = req.params;
    const { firstName, lastName, age } = req.body;
    const user = users.find(user => user.id === userId);

    if(firstName) user.firstName = firstName;
    if(lastName) user.lastName = lastName;
    if(age) user.age = age;

    res.send(`user with id: ${userId} updated successfully.`);
})

export default router;