
curl --help //gives all the available commands

//GET

curl route //return the data that the route is supposted to return

curl -i route //includes header details

curl --head route //return just header info

curl -I route //return just header info

curl -o test.txt route // the data return is copied in the test.txt file

curl -O route // download the data 

//POST

curl -d "name=vineet&lastName=pjp" route // used to pass data to post route
    --data

//PUT

curl -X PUT -d "title=heyYou" route

//DELETE

curl -X DELETE route

//redirected data

curl -L route // if this route is redirected then we will get the data to redirected route

//Accessing Secured route

curl -u userName:Password

//Upload file

curl -T fileToBeUploaded route
