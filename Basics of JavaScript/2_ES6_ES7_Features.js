// ################################ ES6 features ################################

const myDetail = {
    name: 'Yash',
    age: 21,
    hobby: ['video games', 'coding', 'reading', 'not sleeping'],
    social_life: {
        insta: 'www.instagram.com/yash',
        facebook: 'www.facebook.com/yash',
        twitter: 'www.twitter.com/yash'
    }
}

// 1) object destructuring
const { name, age, hobby: hobbies, social_life } = myDetail;

// 2) array destructuring
const [hobby1, hobby2, hobby3, hobby4] = hobbies;

const { insta:instaHandle, facebook:fbHandle, twitter:twitterHandle } = social_life;

// 3) template literals
const mySelf = `My name is ${name}, ${age} years old. My hobbies includes ${hobby1}, ${hobby2}, ${hobby3}, ${hobby4}. My instagram: ${instaHandle}, facebook: ${fbHandle} and twitter: ${twitterHandle}.`

console.log(mySelf);

// 4) fat arrow functions
const myFunction = () => {
    console.log("This is so cool !");
}

const add = (a, b) => a + b;

console.log(add(5, 10));

// 5) class keyword in ES6
class Person {
    constructor(name, age){
        this.name = name;
        this.age = age;
    }
    getDetails() {
        return {
            name: this.name,
            age: this.age
        }
    }
}

// 6) New string methods in ES6
const testString1 = "My name is Yash";
const testString2 = "yolo ";

console.log(testString1.startsWith("My"));
console.log(testString1.endsWith("Yash"));
console.log(testString1.includes("name"));
console.log(testString2.repeat(2));

// 7) let and const data types
// the main use of "let" data type is that it is "block scoped" rather than "function scoped" like var. 
// const is basically when some data is not meant to be changed through out a scope.

// 8) map function
const a = [1,2,3,4,5];
const newArray = a.map((val, index, originalArray) => {
    let temp = 1;
    val += temp;
    return val;
})
console.log(newArray); // [2,3,4,5,6]

// 9) filter method
const temp = [5,12,8,130,44];
const newArr = temp.filter(val => {
    return val > 12;
})
console.log(newArr);

// 10) reduce method
const arr = [1,2,3,4,5];
const arrSum = arr.reduce((accumulator, val) => {
    return accumulator + val;
})
console.log(arrSum);

// ################################ ES7 features ################################

// 1) exponentiation operator
let num1 = 2, num2 = 5;
let result = num1**num2;
console.log(result);

// 2) array prototypes includes method
const testArray = ['yash', 'me', 'is', ':)'];
console.log(testArray.includes('yash'));