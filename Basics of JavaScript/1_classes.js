// class
class Tesla {
    constructor(doors, engine_type) {
        this.doors = doors;
        this.engine_type = engine_type;
    }
}

// inheritance
class TeslaModel extends Tesla {
    constructor(doors, engine_type, model){
        super(doors, engine_type);
        this.model = model;
    }
    get_model() {
        return `Model: ${this.model}`;
    }
}

let tesla1 = new TeslaModel(4, "diesel", "Model S");
let tesla2 = new TeslaModel(2, "petrol", "Model 3");

console.log(tesla1.get_model());
console.log(tesla2.get_model());

// closure
const outerFunction = (a) => {
    let b = 10;
    const innerFunction= () => {
        let sum = a+b;
        console.log(sum);
    }
    return innerFunction();
}

let inner = outerFunction(5);
console.log(inner);

// prototype
class Person {
    constructor(name, age){
        this.name = name;
        this.age = age;
    }
    getDetails(){
        return {
            name: this.name,
            age: this.age
        }
    }
}

Person.prototype.getName = function(){
    return this.name
};

let obj1 = new Person("yash", 21);
console.log(obj1.getName());
