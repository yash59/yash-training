// asynchronous programming

const fun2 = () => {
    setTimeout(() => {
        console.log("Fun 2 starting ...");
    }, 3000);
}

const fun1 = () => {
    console.log("Fun 1 is starting ...");
    fun2();
    console.log("Fun 1 is ending ...");
}

fun1();

// CALLBACKS

// normal version

console.log("start");

const userLogin = (email, pass) => {
    setTimeout(() => {
        return {userEmail: email, password: pass}
    }, 2000)
}

const userDetails = userLogin("yash@bitcs.in", 12345);

console.log(userDetails);

console.log("end");

// USING CALLBACK FUNCTION

console.log("start");

const userLogin = (email, pass, callback) => {
    setTimeout(() => {
        console.log("Data Received !");
        callback({userEmail: email, password: pass})
    }, 2000)
}

const userDetails = userLogin("yash@bitcs.in", 12345, (user) => {
    console.log(user.userEmail);
    console.log(user.password);
});

console.log("end");

// CALLBACK HELL CONDITION (disadvantage of using callbacks)

console.log("start");

const userLogin = (email, pass, callback) => {
    setTimeout(() => {
        console.log("User Data Received !");
        callback({userEmail: email, password: pass})
    }, 3000)
}

const getUserVideos = (userEmail, callback) => {
    setTimeout(() => {
        console.log("User Videos Received !");
        callback(['video1', 'video2', 'video3', 'video4'])
    }, 2000)
}

const userDetails = userLogin("yash@bitcs.in", 12345, (user) => {
    console.log(user);
    const userVideos = getUserVideos(user.userEmail, (videos) => {
        console.log(videos);
    })
});

console.log("end");

// PROMISES (better than callbacks)
// a promise is an object that produces a single value sometime in the future in an asynchronous operation. This object can be a "resolved" value or a "rejected" value.
// clean syntax as compared to callbacks

console.log("start");

const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log("Got the user data !");
        resolve({
            user: "yash"
        })
        reject(new Error("User not logged In !"));
    }, 2000)
})

promise.then(user => {
    console.log(user);
}).catch(err => console.log(err.message));

console.log("end");

// another example if we have to run 2 promises and don't want to wait for 1

const yt = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log("Got videos from youtube");
        resolve({videos: [1,2,3,4,5]});
    }, 3000)
})

const fb = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log("Got user status from facebook");
        resolve({user: "yash"});
    }, 2000)
})

Promise.all([yt, fb]).then(result => console.log(result));

// SYNC - AWAIT syntax
// async await also uses promises but it's syntax is way more simple (synchronous style syntax)

console.log("start");

const userLogin = (email, pass) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log("Data Received !");
            resolve({userEmail: email, password: pass});
            reject(new Error("Could not get user details !"));
        }, 2000)
    })
}

const userDetails = async () => {
    try {
        const result = await userLogin("yash@bitcs.in", 12345);
        console.log(result);
    } catch (error) {
        console.log(error.message)
    }
}

userDetails();

console.log("end");