"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cat = void 0;
class Cat {
    constructor(id, name, age, breed) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}
exports.Cat = Cat;
//# sourceMappingURL=cats.model.js.map