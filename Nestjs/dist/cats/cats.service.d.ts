import { Cat } from './cats.model';
export declare class CatsService {
    cats: Cat[];
    insertCat(name: string, age: number, breed: string): any;
    getAllCats(): Cat[];
    getSingleCat(catId: string): {
        id: string;
        name: string;
        age: number;
        breed: string;
    };
    patchACat(catId: string, name: string, age: number, breed: string): void;
    putACat(catId: string, name: string, age: number, breed: string): void;
    deleteSingleCat(catId: string): void;
}
