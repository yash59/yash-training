import { CatsService } from './cats.service';
export declare class CatsController {
    private readonly catsService;
    constructor(catsService: CatsService);
    addCat(catName: string, catAge: number, catBreed: string): any;
    getAllCats(): import("./cats.model").Cat[];
    getCat(catId: string): {
        id: string;
        name: string;
        age: number;
        breed: string;
    };
    patchCat(catId: string, catName: string, catAge: number, catBreed: string): any;
    putCat(catId: string, catName: string, catAge: number, catBreed: string): any;
    deleteCat(catId: string): any;
}
