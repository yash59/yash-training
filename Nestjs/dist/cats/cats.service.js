"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CatsService = void 0;
const common_1 = require("@nestjs/common");
const uuid_1 = require("uuid");
const cats_model_1 = require("./cats.model");
let CatsService = class CatsService {
    constructor() {
        this.cats = [];
    }
    insertCat(name, age, breed) {
        const catId = uuid_1.v4();
        const newCat = new cats_model_1.Cat(catId, name, age, breed);
        this.cats.push(newCat);
        return catId;
    }
    getAllCats() {
        return [...this.cats];
    }
    getSingleCat(catId) {
        const foundCat = this.cats.find(cat => cat.id === catId);
        if (foundCat)
            return Object.assign({}, foundCat);
        else
            throw new common_1.NotFoundException('Could not find Cat!');
    }
    patchACat(catId, name, age, breed) {
        const foundCat = this.cats.find(cat => cat.id === catId);
        const index = this.cats.findIndex(cat => cat.id === catId);
        const updatedCat = Object.assign({}, foundCat);
        if (foundCat) {
            if (name)
                updatedCat.name = name;
            if (age)
                updatedCat.age = age;
            if (breed)
                updatedCat.breed = breed;
            this.cats[index] = updatedCat;
        }
        else
            throw new common_1.NotFoundException('Could not find Cat!');
    }
    putACat(catId, name, age, breed) {
        const foundCat = this.cats.find(cat => cat.id === catId);
        const index = this.cats.findIndex(cat => cat.id === catId);
        const updatedCat = Object.assign({}, foundCat);
        updatedCat.name = name;
        updatedCat.age = age;
        updatedCat.breed = breed;
        this.cats[index] = updatedCat;
    }
    deleteSingleCat(catId) {
        this.cats = this.cats.filter(cat => cat.id !== catId);
    }
};
CatsService = __decorate([
    common_1.Injectable()
], CatsService);
exports.CatsService = CatsService;
//# sourceMappingURL=cats.service.js.map