export declare class Cat {
    id: string;
    name: string;
    age: number;
    breed: string;
    constructor(id: string, name: string, age: number, breed: string);
}
