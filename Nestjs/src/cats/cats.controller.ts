import { Controller, Get, Post, Delete, Patch, Put, Param, Body } from '@nestjs/common';
import { CatsService } from './cats.service';

@Controller('cats')
export class CatsController {
    constructor(private readonly catsService: CatsService) { }

    // setting up routes
    @Post('')
    addCat(
        @Body('name') catName: string,
        @Body('age') catAge: number,
        @Body('breed') catBreed: string
        ): 
        any {
            const generatedCatId = this.catsService.insertCat(catName, catAge, catBreed);
            return {id: generatedCatId};
        }

    @Get('')
    getAllCats() {
        return this.catsService.getAllCats();
    }

    @Get(':id')
    getCat(@Param('id') catId: string) {
        return this.catsService.getSingleCat(catId);
    }

    @Patch(':id')
    patchCat(
        @Param('id') catId: string,
        @Body('name') catName: string,
        @Body('age') catAge: number,
        @Body('breed') catBreed: string
        ) {
            this.catsService.patchACat(catId, catName, catAge, catBreed);
            return null;
        }

    @Put(':id')
    putCat(
        @Param('id') catId: string,
        @Body('name') catName: string,
        @Body('age') catAge: number,
        @Body('breed') catBreed: string
        )
        {
            this.catsService.putACat(catId, catName, catAge, catBreed);
            return null;
        }

    @Delete(':id')
    deleteCat(@Param('id') catId: string) {
        this.catsService.deleteSingleCat(catId);
        return null;
    }
}