import { Injectable, NotFoundException } from '@nestjs/common';
import { v4 as uuidv4 } from "uuid";
import { Cat } from './cats.model';

@Injectable()
export class CatsService {
    // initialise database here
    cats: Cat[] = [];

    // business logic
    insertCat(name: string, age: number, breed: string) {
        const catId = uuidv4();
        const newCat = new Cat(catId, name, age, breed);
        this.cats.push(newCat);
        return catId;
    }

    getAllCats() {
        return [...this.cats];
    }

    getSingleCat(catId: string) {
        const foundCat = this.cats.find(cat => cat.id === catId);
        if (foundCat) return { ...foundCat };
        else throw new NotFoundException('Could not find Cat!');
    }

    patchACat(catId: string, name: string, age: number, breed: string) {
        const foundCat = this.cats.find(cat => cat.id === catId);
        const index  = this.cats.findIndex(cat => cat.id === catId);
        const updatedCat = {...foundCat};
        
        if (foundCat) {
            if (name) updatedCat.name = name;
            if (age) updatedCat.age = age;
            if (breed) updatedCat.breed = breed;

            this.cats[index] = updatedCat;
        }
        else throw new NotFoundException('Could not find Cat!');
    }

    putACat(catId: string, name: string, age: number, breed: string) {
        const foundCat = this.cats.find(cat => cat.id === catId);
        const index  = this.cats.findIndex(cat => cat.id === catId);
        const updatedCat = {...foundCat};

        updatedCat.name = name;
        updatedCat.age = age;
        updatedCat.breed = breed;

        this.cats[index] = updatedCat;
    }

    deleteSingleCat(catId: string) {
        this.cats = this.cats.filter(cat => cat.id !== catId);
    }
}