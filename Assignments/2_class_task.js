class Person {
    constructor(name, age, salary, sex){
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
    }
    static quickSort(arr, field, order){
        // base case
        if(arr.length <= 1){
            return arr;
        }

        // defining pivot
        const pivot = arr[arr.length-1];

        // using ES6 filter method instead of old for loop
        const leftArr = arr.filter((val) => {
            if(order === 'asc') return val[field] < pivot[field];
            else return val[field] > pivot[field];
        })
        const rightArr = arr.filter((val) => {
            if(order === 'asc') return val[field] > pivot[field];
            else return val[field] < pivot[field];
        })
        
        return [...this.quickSort(leftArr, field, order), pivot, ...this.quickSort(rightArr, field, order)];
    }
}

let person1 = new Person("yash", 21, 5000000, 'male');
let person2 = new Person("abhishek", 23, 1000, 'male');
let person3 = new Person("utkarsh", 22, 100000, 'male');
let person4 = new Person("shubham", 24, 200000, 'male');
let person5 = new Person("bobby", 20, 70000, 'male');

let array = [person1, person2, person3, person4, person5];

let resultSorted = Person.quickSort(array, 'name', 'desc');

console.log("Original Array: ", array);
console.log("Sorted Array: ", resultSorted);
