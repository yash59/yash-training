export {} //in ts, by default a file is a script, to make it a module we need to add export {}

// #################################################################################
// TYPES IN TYPESCRIPT

// string type
let myName: string;
myName = "Yash Kelkar";
console.log(myName);

// number type
let myNumber: number;
myNumber = 10;
console.log(myNumber);

// boolean type
let myBool: boolean;
myBool = true;
console.log(myBool);

// array :- array of strings
let myStringArray: string[];
myStringArray = ['My', 'name', 'is', 'Yash'];
console.log(myStringArray);

// array :- array of numbers
let myNumberArray: number[];
myNumberArray = [1,2,3,4,5];
console.log(myNumberArray);

// array :- array of any data type
let myAnyArray: any[];
myAnyArray = [1,"yash", 23.03, {lname: "kelkar"}];
console.log(myAnyArray);

// #################################################################################
// TYPE ASSERTION IN TYPESCRIPT

let myFullName: any;
myFullName = 'Yash Kelkar';

console.log(myFullName);
// type assertion is used to take advantage of intellisense
console.log(<string>myFullName.charAt(0));
// or
console.log((myFullName as string).length);

// #################################################################################
// FUNCTIONS IN TYPESCRIPT (fat arrow functions)

const myFunc = (fname: string, lname: string): void => {
    console.log(`My full name is: ${fname} ${lname}`);
}

myFunc("Yash", "Kelkar"); // it would throw error if we give anything except strings in args

// #################################################################################
// INTERFACE IN TYPESCRIPT

interface Person {
    fname:string,
    lname:string, 
    age:number, 
    gender:string
}

const details = (person: Person): void => {
    console.log(person.fname);
    console.log(person.lname);
    console.log(person.age);
    console.log(person.gender);
}

const myDetails = {
    fname: "Yash",
    lname: "Kelkar",
    age: 21,
    gender: "Male"
}
details(myDetails);

// #################################################################################
// CLASSES IN TYPESCRIPT

// typescript is object oriented JavaScript.
// a class is a blueprint for creating objects. A class encapsulates data for the object i.e.
// encapsulation is the process of combining data and functions into a single unit called class.

class Person_ {
    // fields
    firstName: string;
    lastName: string;
    age: number;

    // constructor method
    constructor(fName: string, lName: string, age?: number){
        this.firstName = fName;
        this.lastName = lName;
        this.age = age;
    }

    // method
    getDetails(): void {
        console.log(`My name is ${this.firstName} ${this.lastName} and i'm ${this.age} years old.`)
    }
}

let obj1 = new Person_("Yash", "Kelkar");
obj1.getDetails();