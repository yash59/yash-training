"use strict";
exports.__esModule = true;
// #################################################################################
// TYPES IN TYPESCRIPT
// string type
var myName;
myName = "Yash Kelkar";
console.log(myName);
// number type
var myNumber;
myNumber = 10;
console.log(myNumber);
// boolean type
var myBool;
myBool = true;
console.log(myBool);
// array :- array of strings
var myStringArray;
myStringArray = ['My', 'name', 'is', 'Yash'];
console.log(myStringArray);
// array :- array of numbers
var myNumberArray;
myNumberArray = [1, 2, 3, 4, 5];
console.log(myNumberArray);
// #################################################################################
// TYPE ASSERTION IN TYPESCRIPT
var myFullName;
myFullName = 'Yash Kelkar';
console.log(myFullName);
// type assertion is used to take advantage of intellisense
console.log(myFullName.charAt(0));
// or
console.log(myFullName.length);
// #################################################################################
// FUNCTIONS IN TYPESCRIPT (fat arrow functions)
var myFunc = function (fname, lname) {
    console.log("My full name is: " + fname + " " + lname);
};
myFunc("Yash", "Kelkar"); // it would throw error if we give anything except strings in args
var details = function (person) {
    console.log(person.fname);
    console.log(person.lname);
    console.log(person.age);
    console.log(person.gender);
};
var myDetails = {
    fname: "Yash",
    lname: "Kelkar",
    age: 21,
    gender: "Male"
};
details(myDetails);
// #################################################################################
// CLASSES IN TYPESCRIPT
// typescript is object oriented JavaScript.
// a class is a blueprint for creating objects. A class encapsulates data for the object i.e.
// encapsulation is the process of combining data and functions into a single unit called class.
var Person_ = /** @class */ (function () {
    // constructor method
    function Person_(fName, lName, age) {
        this.firstName = fName;
        this.lastName = lName;
        this.age = age;
    }
    // method
    Person_.prototype.getDetails = function () {
        console.log("My name is " + this.firstName + " " + this.lastName + " and i'm " + this.age + " years old.");
    };
    return Person_;
}());
var obj1 = new Person_("Yash", "Kelkar");
obj1.getDetails();
