import Sequelize from 'sequelize';

// making a connection
const connection = new Sequelize(
    "demo_schema", //db name
    "root", // username
    "pwd", //password
    {
        host: '127:0.0.1',
        dialect: 'mysql',
    }
);

// creating a model schema Article in article table
const Article = connection.define("article", {
    slug: { // defining a custom primary id
        type: Sequelize.STRING,
        primaryKey: true // this makes sure we made a custom primary key (slug)
    },
    title: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false, // just like required: true
        validate: { // validation for column "title"
            len: { // custom error message
                args: [10, 150],
                msg: "Please enter a title with atleast 10 characters but not more than 150 characters."
            }
        }
    },
    body: {
        type: Sequelize.TEXT,
        defaultValue: "insert body here ...", // doesn't work on windows so comment it.
        validate: {
            // defining a custom validation
            startsWithUpper: bodyVal => {
                const firstChar = bodyVal.charAt(0);
                var startsWithUpper = firstChar === firstChar.toUpperCase();
                if(!startsWithUpper){
                    throw new Error("First letter must be a uppercase letter.")
                }
            }
        }
    },
}, {
    timestamp: true, // make it false if we dont want created and updated timestamps
    freezeTableName: true, // to avoid sequelize pluralize the table name (article to articles)
    hooks: {
        // defining hooks
        beforeValidate: () => {
            console.log("Before Validate Hook");
        },
        afterValidate: () => {
            console.log("After Validate Hook");
        },
        beforeCreate: () => {
            console.log("Before Create Hook");
        },
        afterCreate: (result) => {
            console.log(`Created article with slug `, result.dataValues.slug);
        }
    }
});

// syncing the model to make sure table exists in db (.sync only creates table if table doesn't exists already.)
connection.sync().then(() => {
        // creating/inserting an article into "demo_schema" db in article table
        Article.create({
            title: "demo title",
            body: "This is my body, usudhiaushdais sudbaisudas d"
        });
        // find an article by its id (primary key)
        Article.findById(3).then((article) => {
            console.log(article.dataValues);
        })
    })
    .catch(error => console.log(error.message));

// for updating data inside table, we cannot use .sync function as it only works for creating tables (or checking if table already there)